package br.com.senac.livraria.bean;

import br.com.senac.livraria.banco.GeneroDAO;
import br.com.senac.livraria.entity.Genero;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

@Named(value = "generoBean")
@RequestScoped
public class GeneroBean {
    
    private Genero genero = new Genero();

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }
    
    private GeneroDAO dao = new GeneroDAO();
    
    public void salvar(){
        try{
            if(this.genero.getId() == 0){
                dao.save(genero);
                
            }
            // salvo com sucesso
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "salvo", "Salvo com sucesso!!!");
            context.addMessage("Oba!!!", message);
            
        }catch(Exception ex){
            // deu erro
        }
    }
    
    public void novo(){
        this.genero = new Genero();
    }
    
    public List<Genero> getLista(){
        return dao.findAll();
    }
    
    
    public GeneroBean() {
    
        
    

}
}


