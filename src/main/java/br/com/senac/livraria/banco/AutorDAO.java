/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.livraria.banco;

import br.com.senac.livraria.entity.Autor;

/**
 *
 * @author Administrador
 */
public class AutorDAO extends DAO<Autor>{
    
    public AutorDAO() {
        super(Autor.class);
    }
    
}
