/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.livraria.banco;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
    
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("LivrariaPU");
    
    
    public static EntityManager getEntityManager(){
        
        try{
            return emf.createEntityManager() ; 
        }catch(Exception ex){
            ex.printStackTrace();
            throw new RuntimeException("Falha ao acessar o banco.");
        }
        
    }
    
    
}
