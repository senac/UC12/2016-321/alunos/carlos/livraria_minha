/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.livraria.entity;

import java.util.concurrent.ExecutionException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Administrador
 */
public class TesteJPA {
    
    public static void main(String... arg) {

        //Fabrica de conexões
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("LivrariaPU");

        //Criar Gestor para as conexões
        //Criar a conexao 
        EntityManager em = emf.createEntityManager();
        
        Genero genero = new Genero();
        genero.setDescricao("Drama");
        
        try {
        
            System.out.println("Codigo:" + genero.getId() + "Descricao: " + genero.getDescricao());
        //Abrindo uma transacao
        em.getTransaction().begin();
        em.persist(genero);
        em.getTransaction().commit(); 
        
            System.out.println("Codigo:" + genero.getId() + "Descricao: " + genero.getDescricao());
        
        } catch (Exception ex){
            em.getTransaction().rollback();
        } finally{
            em.close();
        }
        
    }
    
}
